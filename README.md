# bsec_bme680_aarch64_linux_android

Inspired by https://github.com/alexh-name/bsec_bme680_linux

# Purpose
I need to run BME680 on aarch64 hardware like BananaPi-M64 ( Allwinner A64 - Cortex-A53 - aarch64 )

I also need it to run on both Linux and Android.


# Where did you find aarch64 closed-source library ?
https://community.bosch-sensortec.com/t5/MEMS-sensors-forum/BME680-BSEC-Library-for-ARM64v8-elf64-littleaarch64/m-p/13943/highlight/true#M3359

# Why not use kernel driver from Himanshu Jha ?
I directly asked him about his opinion.
I wish to thank him for his suggestions.

# Why docker ?
Why not? It is much more easier to build aarch64 binaries without intervening your host.

# Building

Run ./build.sh


