#!/bin/bash

set -euo pipefail

DOCKER_IMAGE="bsec_bme680_aarch64_builder"

# If Docker image doesn't exist, build it

if [[ "$(docker images -q ${DOCKER_IMAGE}:latest 2> /dev/null)" == "" ]]; then
	docker build -t ${DOCKER_IMAGE} .
fi

# Run docker container, it will build bsec_bme680_aarch64 binary automatically
docker run -v $(pwd)/src:/opt ${DOCKER_IMAGE} /bin/bash -c 'cd  /opt/ && ./make.sh'

# Move output to output directory
mkdir -p output
mv src/bsec_bme680 output/
mv src/bsec_iaq.config output/
mv src/bsec_iaq.state output/
